print('----------------Meniu----------------')
print('1.Bors     - 40 USD')
print('2.Placinta - 10 USD')
print('3.Compot   - 15 USD')
print ('        Delivery price - 5 USD')
print('---------------------------------------')
print()
print()

#DATA - 
food_1_name = 'Bors'
food_1_price = 40.00

food_2_name = 'Placinta'
food_2_price = 10.00

drink_1_name = 'Compot'
drink_1_price = 15.00

delivery_price = 5

#DATA - input
food_1_ask = input ('Do you want Bors? (yes/no)  ')
if food_1_ask == 'yes' :
  print ('You select Bors')
  food_1_q = int(input(f'{f"How much {food_1_name} want?":25}' ))
  food_1_price_order = food_1_price * food_1_q
  print (f'You must to pay for {food_1_name} {food_1_price_order} USD')
else :
  print(f"You don't want {food_1_name}")
  food_1_price_order = 0

print()

food_2_ask = input ('Do you want Placinta? (yes/no)  ')
if food_2_ask == 'yes' :
  print ('You select Placinta')
  food_2_q = int(input(f'{f"How much {food_2_name} want?":25}' ))
  food_2_price_order = food_2_price * food_2_q
  print (f'You must to pay for {food_2_name} {food_2_price_order} USD')
else :
  print(f"You don't want {food_2_name}")
  food_2_price_order = 0

print()

drink_1_ask = input (f'Do you want {drink_1_name}? (yes/no)  ')
if drink_1_ask == 'yes' :
  print (f'You select {drink_1_name}')
  drink_1_q = int(input(f'{f"How much {drink_1_name} want?":25}' ))
  drink_1_price_order = drink_1_price * drink_1_q
  print (f'You must to pay for {drink_1_name} {drink_1_price_order} USD')
else :
  print(f"You don't want {drink_1_name}")
  drink_1_price_order = 0

print()

print()

if food_1_ask == 'no' and food_2_ask == 'no' and drink_1_ask == 'no' :
  print("You don't select any product")
  exit()



# COST

total_price = food_1_price_order + food_2_price_order + drink_1_price_order

if total_price >= 200 :
  print(f'Free delivery. Total price for order is {total_price}  USD')
else :
  total_price_delivery = total_price + delivery_price
  print(f'Total price for order is {total_price_delivery}  USD  +  delivery')